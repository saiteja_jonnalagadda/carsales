f=open('data.csv','r')#Opens the data.csv file
o=open('Mapper4Output.txt','w') #writes the output to the Mapper4Output.txt
start = True
for data in f:
    input=data.strip().split(';')
    if len(input)==12:
        ID,make,model,version,months_old,power,sale_type,num_owners,gear_type,fuel_type,kms,price=input
        if not start and ID!="months_old":
            if len(months_old)!=0 and len(power) != 0:
                o.write(months_old+","+power+"\n" )#writes the months_old and power to the output file
        else:
           start = False

f.close()
o.close()