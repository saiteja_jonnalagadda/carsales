i=open('sorted3Output.txt','r')   # opens sorted3Output as read file
o=open('reducer3Output.txt','w')  #opens reducer3Output file as writable file

# Finding count of cars having price less than 70,000 and driven 10,000 kms

MAXMIL=10000;
MAXPRIC=70000;
temprc=""
tempr=0
total = 0
totalPrice = 0
for line in i:
	data = line.strip().split(",")
	rc,r = data
	if int(rc) < MAXMIL and int(r) < MAXPRIC:
		totalPrice += int(r)
		total += 1
		if temprc == "":
			temprc = rc
			tempr+=1
		else:
			if temprc == rc:
				tempr+=1
			elif temprc!=rc:
					print (temprc +"\t"+ str(tempr))
					o.write(temprc +"\t"+ str(tempr)+"\n")
					temprc = rc
					tempr = 0
print (temprc +"\t"+ str(tempr)) #prints list of cars in range
o.write(temprc +"\t"+ str(tempr)+"\n")
#gives count of required range
o.write("Total number of cars having price less than 70,000 and driven less than 10,000 miles is : "+str(total)+"\n") 
# o.write("Average of the above mentioned range is :"+str(totalPrice/total))