f=open('data.csv','r')    #reads data.csv as f
o=open('Mapper3Output.txt','w') #opens writable file as Mapper3Output.txt
start = True
for data in f:
    input=data.strip().split(';') #splits data seperated by ";"
    if len(input)==12:
        ID,make,model,version,months_old,power,sale_type,num_owners,gear_type,fuel_type,kms,price=input # assigns values
        if not start and ID!="kms":
            if len(kms)!=0 and len(price) != 0:
                o.write(kms+","+price+"\n" )  # writes kms and price as key value pairs in Mapper output file
        else:
           start = False

f.close()   #closes file f
o.close() #closes file o
    