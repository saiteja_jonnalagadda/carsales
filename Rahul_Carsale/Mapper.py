i = open("data.csv", "r") # open file, read-only
o = open("map.txt", "w")# open file, write

for line in i:

    # It splits the each line of code by ;
  data = line.strip().split(';')
  
  #It checks Whether their are 12 variables
  if (len(data) == 12):

    ID, make, model, version, months_old, power, sale_type, num_owners, gear_type, fuel_type, kms, price = data
    o.write(make + "\t" + str(1) + "\n")

i.close()
o.close()